variable "project-id" {
  type        = string
  description = "Google Project ID"
}

variable "region" {
  type        = string
  description = "Google cloud region"
  default     = "us-central1"
}
